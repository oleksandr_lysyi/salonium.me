$('.nav-tabs a').click(function (e) {
  e.preventDefault();
  $(this).tab('show');
});

$(".show-tooltip").tooltip({
  placement: 'bottom'
});

// $(".show-modal").modal('show');

$('.popover-map').popover({
  placement: 'right',
  container: 'body',
  html: true,
  content: function () {
      return $(this).next('.popover-map-container').html();
  }
});

